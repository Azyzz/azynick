package com.john55223.azyzz.main;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.john55223.azyzz.cmd.Nick;
import com.john55223.azyzz.listener.ChatEvents;
import com.john55223.azyzz.util.PlayerDataManager;

public class AzyNick extends JavaPlugin
{
	private HashMap<UUID,String> nicks = new HashMap<UUID, String>();
	private PlayerDataManager pdm;
	private int saveTask;
	
	public void onEnable()
	{
		
		getCommand("nick").setExecutor(new Nick(this));
		getServer().getPluginManager().registerEvents(new ChatEvents(this), this);
		
		loadNicks();
		
		saveTask = Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {

			@Override
			public void run() 
			{
				saveNicks();
				//System.out.println("[AzyNick] Saved Nicknames!");
			}
			
		}, 6000L, 6000L);
	}
	
	public void onDisable()
	{
		saveNicks();
		Bukkit.getScheduler().cancelTask(saveTask);
	}


	public HashMap<UUID, String> getNicks()
	{
		return nicks;
	}
	public void setNicks(HashMap<UUID,String> nicks)
	{
		this.nicks = nicks;
	}
	public void updateNick(OfflinePlayer offlinePlayer, String nick)
	{
		UUID uuid = offlinePlayer.getUniqueId();
		if(!(this.nicks.containsKey(uuid)))
		{
			this.nicks.put(uuid, nick);
		}
		else
		{
			this.nicks.remove(uuid);
			this.nicks.put(uuid, nick);
		}
	}
	public void deleteNick(Player p)
	{
		if(this.nicks.containsKey(p.getUniqueId()))
		{
			this.nicks.remove(p.getUniqueId());
		}
	}
	
	
	public void loadNicks() {
		pdm = new PlayerDataManager(this);
		nicks.clear();
		if((pdm.getConfigsInDir().isEmpty())) return;
		for(YamlConfiguration config : pdm.getConfigsInDir())
		{
			UUID uuid = UUID.fromString(config.getString("UUID"));
			String nick = config.getString("Nickname");
			updateNick(Bukkit.getOfflinePlayer(uuid), nick);
		}
		
	}
	public void saveNicks()
	{
		pdm = new PlayerDataManager(this);
		if(nicks == null || nicks.isEmpty()) return;
		for(Map.Entry<UUID, String> set : nicks.entrySet())
		{
			Player p = Bukkit.getPlayer(set.getKey());
			if(p == null)
			{
				//System.out.println("[AzyNick] ERROR: Player Null while saving nickname for: " + set.getKey().toString());
				continue;
			}
			YamlConfiguration config = pdm.load(p);
			config.set("UUID", p.getUniqueId().toString());
			config.set("Nickname", nicks.get(p.getUniqueId()));
			pdm.save(p, config);
		}
	}
}
