package com.john55223.azyzz.cmd;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.john55223.azyzz.main.AzyNick;

import net.md_5.bungee.api.ChatColor;

public class Nick implements CommandExecutor
{
	AzyNick pl;
	public Nick(AzyNick pl)
	{
		this.pl = pl;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandlabel, String[] args) 
	{
		if(!(cmd.getName().equalsIgnoreCase("nick"))) return false;
		
		if(args.length == 0 || args[0].equalsIgnoreCase("help"))
		{
			sender.sendMessage(ChatColor.DARK_GREEN + "/nick <nickname> " + ChatColor.GOLD + "- Set a nickname for yourself.");
			sender.sendMessage(ChatColor.DARK_GREEN + "/nick <nickname> <player>" + ChatColor.GOLD + "- Give someone else a nickname.");
			sender.sendMessage(ChatColor.DARK_GREEN + "/nick clear" + ChatColor.GOLD + "- Reset your nickname.");
			sender.sendMessage(ChatColor.DARK_GREEN + "/nick clear <player> " + ChatColor.GOLD + "- Reset a nickname.");
			return false;
		}
		else if(args[0].equals("clear"))
		{
			// /nick clear
			if(args.length == 1)
			{
				if(!sender.hasPermission("azynick.set.other"))
				{
					sender.sendMessage(ChatColor.DARK_RED + "You don't have permission for this!");
					return false;
				}
				
				if(!(sender instanceof Player))
				{
					sender.sendMessage("Usage: /nick clear <playername>");
					return false;
				}
				Player p = (Player) sender;
				pl.deleteNick(p);
				p.sendMessage(ChatColor.GREEN + "Successfully cleared your nickname!");
			}
			else if(args.length == 2)
			{
				Player p = Bukkit.getPlayer(args[1]);
				if(p == null)
				{
					sender.sendMessage(ChatColor.DARK_RED + "Invalid Player!");
					return false;
				}
				pl.deleteNick(p);
				sender.sendMessage(ChatColor.GREEN + "Successfully cleared " + p.getName() + "'s nickname!");
				return true;
			}
		}
		// /nick <nickname>
		else if(args.length == 1)
		{
			Player p = (Player) sender;
			if(!p.hasPermission("azynick.set"))
			{
				p.sendMessage(ChatColor.DARK_RED + "You don't have permission for this!");
				return false;
			}
			pl.updateNick(p, args[0]);
			p.sendMessage(ChatColor.GREEN + "Successfully updated nickname!");
		}
		else if(args.length == 2)
		{
			if(!sender.hasPermission("azynick.set.other"))
			{
				sender.sendMessage(ChatColor.DARK_RED + "You don't have permission for this!");
				return false;
			}
			Player p = Bukkit.getPlayer(args[1]);
			if(p == null)
			{
				sender.sendMessage(ChatColor.DARK_RED + "Invalid Player!");
				return false;
			}
			pl.updateNick(p, args[0]);
			sender.sendMessage(ChatColor.GREEN + "Successfully updated nickname!");
		}
		else
		{
			sender.sendMessage(ChatColor.DARK_RED + "Usage: /nick help");
			return false;
		}
		return false;
	}

}
