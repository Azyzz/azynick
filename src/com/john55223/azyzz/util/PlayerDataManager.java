package com.john55223.azyzz.util;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import com.john55223.azyzz.main.AzyNick;


public class PlayerDataManager 
{
	AzyNick plugin;
	File dir;
	
	public PlayerDataManager(AzyNick pl)
	{
		plugin = pl;
		dir = new File(plugin.getDataFolder() + "/players");
		if(!dir.exists())
		{
			dir.mkdirs();
		}
	}
	
	public YamlConfiguration load(Player p)
	{
		File cFile = new File(dir + "/" + p.getUniqueId() + ".yml");
		
		if(!cFile.exists())
		{
			try{
				cFile.createNewFile();
			} catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return YamlConfiguration.loadConfiguration(cFile);
	}
	public File getDir()
	{
		return dir;
	}
	public List<File> getCFilesInDir()
	{
		return Arrays.asList(dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name)
			{
				return name.toLowerCase().endsWith(".yml");
			}
		}));
	}
	public List<YamlConfiguration> getConfigsInDir()
	{
		List<YamlConfiguration> configs = new ArrayList<YamlConfiguration>();
		if(!getCFilesInDir().isEmpty()){
		
			for(File f : getCFilesInDir())
			{
				configs.add(YamlConfiguration.loadConfiguration(f));
			}
		}
		return configs;
	}
	public File getCFile(Player p)
	{
		return new File(dir + "/" + p.getUniqueId() + ".yml");
	}
	public void save(Player p, YamlConfiguration config)
	{
		try {
			config.save(getCFile(p));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
