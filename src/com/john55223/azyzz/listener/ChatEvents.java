package com.john55223.azyzz.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import com.john55223.azyzz.main.AzyNick;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.HoverEvent.Action;
import net.md_5.bungee.api.chat.TextComponent;

public class ChatEvents implements Listener
{
	AzyNick pl;
	public ChatEvents(AzyNick pl)
	{
		this.pl = pl;
	}
	
	@EventHandler
	public void onChat(AsyncPlayerChatEvent e)
	{
		String nick = pl.getNicks().get(e.getPlayer().getUniqueId());
		if(nick == null)
		{
			nick = e.getPlayer().getName();
		}
		nick = ChatColor.translateAlternateColorCodes('&', nick);
		String pname = e.getPlayer().getName();
		
		TextComponent name = new TextComponent(ChatColor.WHITE + "<" + nick + ChatColor.WHITE + "> ");
		name.setHoverEvent(new HoverEvent(Action.SHOW_TEXT, new ComponentBuilder(pname).color(ChatColor.BLUE).create()));
		
		TextComponent msg = new TextComponent(e.getMessage());
		
		e.setCancelled(true);
		
		for(Player p : e.getRecipients())
		{
			p.spigot().sendMessage(name, msg);
		}
	}
}
